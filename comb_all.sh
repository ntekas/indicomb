#!/bin/bash

python3 /afs/cern.ch/user/n/ntekas/work/Indicomb/indicomb/nswtalks_atlas_weekly.py
mv -f /afs/cern.ch/user/n/ntekas/work/Indicomb/indicomb/AtlasWeekly_NswTalks.html /eos/user/n/ntekas/www/

python3 /afs/cern.ch/user/n/ntekas/work/Indicomb/indicomb/muonnswtalks_atlas_week.py
mv -f /afs/cern.ch/user/n/ntekas/work/Indicomb/indicomb/AtlasWeek_NswTalks.html /eos/user/n/ntekas/www/

python3 /afs/cern.ch/user/n/ntekas/work/Indicomb/indicomb/l0muon_mdt_talks.py
mv -f /afs/cern.ch/user/n/ntekas/work/Indicomb/indicomb/l0muon_mdt_Talks.html /eos/user/n/ntekas/www/

python3 /afs/cern.ch/user/n/ntekas/work/Indicomb/indicomb/l0muon_sl_talks.py
mv -f /afs/cern.ch/user/n/ntekas/work/Indicomb/indicomb/l0muon_sl_Talks.html /eos/user/n/ntekas/www/

python3 /afs/cern.ch/user/n/ntekas/work/Indicomb/indicomb/l0muon_talks.py
mv -f /afs/cern.ch/user/n/ntekas/work/Indicomb/indicomb/l0muon_Talks.html /eos/user/n/ntekas/www/
