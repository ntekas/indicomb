#!/usr/bin/env python

from indicomb import indicomb

indicomb( API_KEY="7636ac7c-46c3-4cc3-b5e4-3da385ccf9b3" , SECRET_KEY="298f13dc-1e61-4aaa-9351-63b2af652cc8",
          headerHTML="<center><h2>ATLAS Weekly NSW Talks</h2></center>",
          calendarName=None,
          output="/afs/cern.ch/user/n/ntekas/work/Indicomb/indicomb/AtlasWeekly_NswTalks.html",
          includeList=["ATLAS"],
          contributionList=["NSW", "New Small Wheel", "MMG", "Micromegas", "STG", "STGC", "sTGC"],
          excludeList=["Postponed", "POSTPONED", "CANCELLED","CANCELED"],
          startDate="2013-01-01",
          categoryNumbers=[2636])

